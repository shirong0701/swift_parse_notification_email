//
//  ViewController.swift
//  Notification_Email
//
//  Created by Alexander on 06/12/15.
//  Copyright © 2015 Alexander. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //Outlets
    
    @IBOutlet weak var btnSignUp: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSignUp.layer.cornerRadius = 5
        btnSignUp.layer.borderColor = UIColorFromRGB(0x000000, alpha: 0.7).CGColor
        btnSignUp.layer.borderWidth = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

