//
//  SignUpViewController.swift
//  Notification_Email
//
//  Created by Alexander on 06/12/15.
//  Copyright © 2015 Alexander. All rights reserved.
//

import UIKit
import SVProgressHUD
import Parse

class SignUpViewController: UIViewController {
    
    //Outlets
    
    @IBOutlet weak var txtFirstname: UITextField!
 
    @IBOutlet weak var txtLastname: UITextField!
    
    @IBOutlet weak var txtUsername: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!

    @IBOutlet weak var txtPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func tapBtnSignUp(sender: AnyObject) {
        let user = User()
        
        user.firstname = txtFirstname.text
        user.lastname = txtLastname.text
        user.email = txtEmail.text
        user.username = txtUsername.text
        user.password = txtPassword.text
        
        SVProgressHUD.showWithStatus("Processing Signup")
        
        user.signUpInBackgroundWithBlock { (succeeded: Bool!, error: NSError?) -> Void in
            if(succeeded == true)
            {
                SVProgressHUD.showSuccessWithStatus("Success SignUp")
                self.txtFirstname.text = ""
                self.txtLastname.text = ""
                self.txtUsername.text = ""
                self.txtEmail.text = ""
                self.txtPassword.text = ""
                
                let installation = PFInstallation.currentInstallation()
                installation["user"] = user
                installation.saveInBackground()
            }
            else
            {
                SVProgressHUD.showErrorWithStatus("Error SignUp, Please retry")
                print("error: \(error)")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
