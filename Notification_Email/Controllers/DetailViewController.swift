//
//  DetailViewController.swift
//  Notification_Email
//
//  Created by Alexander on 06/12/15.
//  Copyright © 2015 Alexander. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    // Outlets
    
    @IBOutlet weak var lblUsername: UILabel!
    
    @IBOutlet weak var lblUserEmail: UILabel!
    
    // Properties
    
    var selectedUser:User?

    override func viewDidLoad() {
        super.viewDidLoad()

        lblUsername.text = self.selectedUser?.username
        lblUserEmail.text = self.selectedUser?.email
        
        let rightButtton = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.Plain, target: self, action: "tapBtnSend")
        self.navigationItem.rightBarButtonItem = rightButtton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tapBtnSend()
    {
        print("tapBtnSend")
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
