//
//  UserListViewController.swift
//  Notification_Email
//
//  Created by Alexander on 06/12/15.
//  Copyright © 2015 Alexander. All rights reserved.
//

import UIKit
import Parse
import SVProgressHUD
import AHKActionSheet
import IBActionSheet

class UserListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, IBActionSheetDelegate {
    
    // Outlets
    
    @IBOutlet weak var tblUserList: UITableView!
    
    // Properties
    
    var userList:NSMutableArray?
    var selectedIndex:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
         self.automaticallyAdjustsScrollViewInsets = false;
        
        self.userList = NSMutableArray()
        
        getUserList()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableView DataSource and Delegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.userList?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cell")
        if cell == nil
        {
            cell = UITableViewCell.init(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let user:User? = self.userList?.objectAtIndex(indexPath.row) as? User
        
        cell?.textLabel?.text = "\(user!.firstname!) \(user!.lastname!)"
        cell?.detailTextLabel?.text = user?.email

        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView .deselectRowAtIndexPath(indexPath, animated: true)
        
        self.selectedIndex = indexPath.row
        
        let actionSheet = IBActionSheet.init(title: "Select", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitlesArray: ["Send Notification", "Send Email"])
        
        actionSheet.showInView(self.view)
    }
    
    // MARK: - IBActionSheet Delegate Methods
    
    func actionSheet(actionSheet: IBActionSheet!, clickedButtonAtIndex buttonIndex: Int) {
        
    }
    
    func actionSheet(actionSheet: IBActionSheet!, didDismissWithButtonIndex buttonIndex: Int) {
        switch buttonIndex
        {
        case 0:
            let user = self.userList?.objectAtIndex(self.selectedIndex!)
            let query = PFInstallation.query()
            query?.whereKey("user", equalTo: user!)
            
            let data = [
                "alert" : "sending push notification",
                "badge" : "Increment",
                "sounds" : "cheering.caf"
            ]
            
            let push = PFPush()
            push.setQuery(query)
            push.setMessage("sending message via push notification")
//            push.setData(data)
//            push.sendPushInBackground()
            push.sendPushInBackgroundWithBlock({ (status:Bool, error: NSError?) -> Void in
                print(error?.description)
            })
            
        case 1:
            self.performSegueWithIdentifier("showDetailPage", sender: nil)
        default:
            print("other")
        }
    }
    
    // MARK: - Helpers
    
    func getUserList()
    {
        SVProgressHUD .showWithStatus("Loading")
        
        let query = User.query()
        
        query?.findObjectsInBackgroundWithBlock { (users: [PFObject]?, error: NSError?) -> Void in
            if error == nil
            {
                SVProgressHUD .dismiss()
                self.userList?.addObjectsFromArray(users!)
                self.tblUserList.reloadData()
            }
            else
            {
                SVProgressHUD .showErrorWithStatus("Network Error! Please retry")
            }
        }
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetailPage"
        {
            let vc:DetailViewController? = segue.destinationViewController as? DetailViewController
            vc?.selectedUser = self.userList?.objectAtIndex(self.selectedIndex!) as? User
        }
    }
}
