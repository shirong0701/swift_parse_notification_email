//
//  Constants.swift
//  Notification_Email
//
//  Created by Alexander on 06/12/15.
//  Copyright © 2015 Alexander. All rights reserved.
//

import Foundation
import UIKit

let PARSE_APP_ID = "O9RBp3vNIklFfyRuBadQf8knw4dTtFcOTePHGby4"
let PARSE_CLIENT_KEY = "KY5Pu6bjEvbrJQtgkc0fcjhuHKZksVGdGOkaSb9m"

func UIColorFromRGB(rgbValue: UInt, alpha: CGFloat) -> UIColor {
    return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: alpha)
}