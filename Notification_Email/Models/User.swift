//
//  User.swift
//  Notification_Email
//
//  Created by Alexander on 06/12/15.
//  Copyright © 2015 Alexander. All rights reserved.
//

import UIKit
import Parse

class User: PFUser {

    @NSManaged var firstname: String?
    @NSManaged var lastname: String?
    
    override init() {
        super.init()
    }
}
